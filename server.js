
require('dotenv').config();

const express = require('express');
const app=express();
const port = process.env.PORT || 3000;

app.use(express.json());

const userController =require('./controllers/UserController');
const authController =require('./controllers/AuthController');

app.listen(port);
console.log("API escruchando en el puerto "+ port);
app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Recibido mensaje"});
 }
)

//app.get('/apitechu/v1/users',
//function(req,res){
  //console.log("GET /apitechu/vi/users");
  //res.sendFile('usuarios.json',{root: __dirname});

  //var users=require('./usuarios.json');
  //res.send(users);
//}
//)

app.get('/apitechu/v1/users',userController.getUsersV1);
//para sacar los usuarios de la base de datos
app.get('/apitechu/v2/users',userController.getUsersV2);
//llamada para conseguir usuario con un id específico
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);
//llamada para conseguir usuario con un id específico
app.post('/apitechu/v1/users',userController.createUserV1);
app.post('/apitechu/v2/users',userController.createUserV2);

app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);


app.post('/apitechu/v1/login', authController.loginUserV1);
app.post('/apitechu/v2/login', authController.loginUserV2);

app.post('/apitechu/v1/logout/:id', authController.logoutUserV1);
app.post('/apitechu/v2/logout/:id', authController.logoutUserV2);


app.post("/apitechu/v1/monstruo/:p1/:p2",
function (req,res){
  console.log("Parámetros");
  console.log(req.params);

  console.log ("Query String");
  console.log (req.query)

  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);

}
)
