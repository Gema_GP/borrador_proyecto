const io=require('../io');
const requestJson =require('request-json');

const crypt = require ('../crypt');

const baseMLabURL ="https://api.mlab.com/api/1/databases/apitechugr10ed/collections/";
const mLabAPIKey="apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res) {
 console.log("GET /apitechu/v1/users");

 var result = {};
 var users = require('../usuarios.json');

 if (req.query.$count == "true") {
   console.log("Count needed");
   result.count = users.length;
 }

 result.users = req.query.$top ?
   users.slice(0, req.query.$top) : users;

 res.send(result);
}

function getUsersV2(req, res) {
 console.log("GET /apitechu/v2/users");

 var httpClient = requestJson.createClient(baseMLabURL);
 console.log ("Client Created");

 httpClient.get("user?" +mLabAPIKey,
    function(err,resMLab,body){
      var response =!err? body:{
        "msg": "Error obteniendo usuarios"
      }
      res.send(response);
    }
 )
}
//funcion conseguir un usuario de v2

function getUserByIdV2(req,res){
  console.log("GET /aptitechu/v2/users/:id");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log ("Client Created");

  //var query="q={id:"+req.params.id +"}&";
  var id=req.params.id;
  var query='q={"id":'+ id +'}'; //sacar usuario con un id
  console.log ("La consulta es" + query);

  httpClient.get("user?" + query + "&"+ mLabAPIKey,
     function(err,resMLab,body){

          if (err){
            var response= {
              "msg": "Error obteniendo usuarios"
            }
            res.status(500);
          } else {
            if (body.length > 0){
              var response = body[0];
            } else {
              var response ={
                "msg": "Usuario no encontrado"
              }
              res.status(404);
            }
       }
       res.send(response);
     }
  )
}



//función incluir usuario: hemos creado constante io(modulo donde está el writeUserdatafile)
function createUserV1(req,res){
    console.log("POST /apitechu/vi/users");

    //console.log(req.body);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var users =require('../usuarios.json');
    var idNew = 0;
       users.forEach(function(item){
         idNew =  (item.id > idNew) ? item.id : idNew;
       })

    var newUser={
      "id":idNew +1,
      "first_name": req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email
    }

    //var users =require('../usuarios.json');
    users.push (newUser);
    io.writeUserDatatoFile (users);

    console.log("Usuario añadido con éxito");
    res.send ({"msg": "Usuario añadido on exito"})
  }


//Insertar documento en mongo
function createUserV2 (req,res) {
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.pass);
  console.log(req.body.ip_address);

  var newUser={
    "id":req.body.id,
    "first_name": req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "pass": crypt.hash(req.body.pass),
    "ip": req.body.ip_address
  }

  var httpClient=requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKey,newUser,
    function (err,resMLab,body){
      console.log("Usuario Creado en Mlab");
      res.status(201).send({"msg":"Usuario guardado"});
    }
  )


}




  function deleteUserV1(req,res){
    console.log("DELETE /apitechu/v1/users:id");
    console.log("id es " + req.params.id);

    var users =require('../usuarios.json');
    var deleted = false;
    console.log ("usando for of 2");
    for (var [index, user] of users.entries()) {
       console.log("comparando " + user.id + " y " +  req.params.id);
         if (user.id == req.params.id) {
           console.log("La posicion " + index + " coincide");
           users.splice(index, 1);
           deleted = true;
           break;
         }

       }
       if (deleted) {
      io.writeUserDatatoFile (users);
          }

   var msg = deleted ?
     "Usuario borrado" : "Usuario no encontrado."

   console.log(msg);
   res.send({"msg" : msg});
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2=getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 =createUserV2;
module.exports.deleteUserV1 = deleteUserV1;


//MI CODIGO DE GET.APP conseguir lista de usuarios
/*function getUsersV1 (req,res){
  console.log("GET /apitechu/vi/users");
  console.log(req.query);
  console.log(req.query.$count);
  console.log(req.query.$top);

  var respuesta ={};
  var users=require('../usuarios.json');

  if (req.query.$count =='true') {
    respuesta.count=users.length;
    console.log(respuesta.count);
  }

  if (req.query.$top){
    respuesta.users=users.slice(0,req.query.$top);
    console.log (respuesta.top)
  } else {
    respuesta.users = users;
  }
    res.send(respuesta);
}

module.exports.getUsersV1 = getUsersV1;
*/
