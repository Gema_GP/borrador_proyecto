const io=require('../io');
const requestJson =require('request-json');

const crypt = require ('../crypt');
const baseMLabURL ="https://api.mlab.com/api/1/databases/apitechugr10ed/collections/";
const mLabAPIKey="apiKey=" + process.env.MLAB_API_KEY;



function loginUserV1 (req,res){
  console.log("POST /apitechu/v1/login");
    console.log(req.body.email);
    console.log(req.body.pass);

  var users =require('../usuarios.json');
  var indice = -1;

  for (var i = 0; i < users.length; i++){

  if (users[i].email==req.body.email && users[i].pass==req.body.pass){

    console.log ("son iguales");
    indice = i;
    break;
  }

  }
  if (indice != -1){
    users[indice].logged = true;
    io.writeUserDatatoFile (users);
    res.send({"msg" : "Login correcto", "idUsuario":users[indice].id });
  }else{

    res.send({"msg" : "Login incorrecto"});
  }

}

function loginUserV2 (req,res){
  console.log("POST /apitechu/v2/login");
  var httpClient = requestJson.createClient(baseMLabURL);

  console.log(req.body.email);
  console.log(req.body.pass);

  var query = 'q={"email": "' + req.body.email + '"}';

  console.log ("La consulta es" + query);

  httpClient.get("user?" + query + "&"+ mLabAPIKey,
     function(errLoged,resMLabLoged,bodyLoged){
      console.log (bodyLoged);
      console.log (bodyLoged[0].pass);

      if (crypt.checkPassword(req.body.pass, bodyLoged[0].pass)) {

        console.log ("son iguales") ;

        var putBody = '{"$set":{"logged":true}}';

        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errLogedp,resMLabLogedp,bodyLogedp){

             if (errLogedp) {
               console.log ("Hay error")
               var response= {
                 "msg": "Error grabando logged true"
               }
               res.status(401);
               res.send (response);

             } else {

               var response = {
                  "msg": "Usuario correcto",
                  "id": bodyLoged[0].id
               }
               res.send (response);
             }

           }
      )

      } else {
        console.log ("NOOOOOO son iguales") ;
      }
  }
)
}


function logoutUserV1 (req,res){
  console.log("POST /apitechu/v1/logout/:id");

  var users =require('../usuarios.json');
  var indice = -1;

  for (var i = 0; i < users.length; i++){

  if (users[i].id==req.params.id &&  users[i].logged==true){

    console.log ("son iguales");
    indice = i;
    break;
  }

  }
  if (indice != -1){

    delete users[indice].logged;
        io.writeUserDatatoFile (users);

    res.send({"msg" : "Logout correcto", "idUsuario":users[indice].id });
  }else{

    res.send({"msg" : "Logout incorrecto"});
  }

}

function logoutUserV2 (req,res){
  console.log("POST /apitechu/v2/logout/:id");
  var httpClient = requestJson.createClient(baseMLabURL);

  console.log(req.params.id);

  var query = 'q={"id": ' + req.params.id + '}';
  console.log(query);

  //var putBody = '{"$unset":{"logged":""}}';
  //console.log(putBody );

  httpClient.get("user?" + query + "&"+ mLabAPIKey,
     function(errLogout,resMLabLogout,bodyLogout){
      console.log (bodyLogout);
      //console.log (bodyLogout[0].id);

        if(bodyLogout[0].id ==req.params.id && bodyLogout[0].logged){

        console.log ("Usuario existe y está logado");
        var putBody = '{"$unset":{"logged":""}}';

        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
        function(errLogoutp,resMLabLogoutp,bodyLogoutp){

          if (errLogoutp){
            var response = {
              "msg" : "error al borrar logged"
            }
            res.status(500);
          res.send (response);;
          } else {
            var response ={
              "msg" :  "Usario deslogado"
            }
            res.send (response);
          }


        }
)

      } else {

        console.log ("NOOOOOO está logado");
      }
}
)
}



module.exports.loginUserV1 = loginUserV1;
module.exports.loginUserV2 = loginUserV2;

module.exports.logoutUserV1 = logoutUserV1;
module.exports.logoutUserV2 = logoutUserV2;
